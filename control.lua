local event_listener = require('__event-listener__/branch-2/stable-version')
local modules = {}
modules.forgotten_fish = require("forgotten-fish/control")

local fish_tick = require("forgotten-fish/config").fish_tick
script.on_nth_tick(fish_tick, modules.forgotten_fish.on_nth_tick_check)
event_listener.add_events(modules)
