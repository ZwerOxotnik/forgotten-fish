--[[
Copyright (c) 2019 ZwerOxotnik <zweroxotnik@gmail.com>, lukser
Licensed under the MIT licence;
Authors: ZwerOxotnik, lukser
Version: 1.2.0 (2019.07.01)

You can write and receive any information on the links below.
Source: https://gitlab.com/ZwerOxotnik/forgotten-fish
Mod portal: https://mods.factorio.com/mod/forgotten-fish
Homepage: https://forums.factorio.com/viewtopic.php?f=190&t=64778
]]--

local MAX_AFK_TIME = require("forgotten-fish/config").fish_tick + 60
local enemies = require("forgotten-fish/config").enemies
local mod = {}

local function get_enemy()
  local evolution_factor = game.forces['enemy'].evolution_factor

  local enemy_max_lvl
  local enemy_min_lvl = 1
  local enemy_max_size
  local enemy_min_size = 1

  if evolution_factor > 0.95 then
    enemy_max_lvl = #enemies
    enemy_max_size = 4
  elseif evolution_factor > 0.6 then
    enemy_max_lvl = 4
    enemy_max_size = 3
  elseif evolution_factor > 0.25 then
    enemy_max_lvl = 3
    enemy_max_size = 2
  else
    enemy_max_lvl = 2
    enemy_max_size = 1
  end

  local enemy_number = math.random(enemy_min_lvl, enemy_max_lvl)

  if not enemies[enemy_number].sizes then
    return tostring(enemies[enemy_number].name)
  end
  if enemy_max_size > #enemies[enemy_number].sizes then
    return tostring(enemies[enemy_number].sizes[math.random(enemy_min_size, #enemies[enemy_number].sizes)] .. '-' .. enemies[enemy_number].name)
  end

  return tostring(enemies[enemy_number].sizes[math.random(enemy_min_size, enemy_max_size)] .. '-' .. enemies[enemy_number].name)
end

local function check_fish(event)
  if math.random(100) < 50 then return end

  -- Validation of data
  local entity = event.entity
  if entity.name ~= 'fish' then return end
  local target
  if event.player_index then
    target = game.players[event.player_index]
    if not (target and target.valid and target.character and not target.cheat_mode) then return end
  else
    target = event.robot
    if not (target and target.valid) then return end
  end
  if target.surface.get_tile(target.position).name:find("water") ~= nil then return end

  local enemy = get_enemy()
  local position = target.surface.find_non_colliding_position(enemy, entity.position, 5, 0.01)
  if not position then return end
  target.surface.create_entity{name = enemy, position = position}
end

mod.on_nth_tick_check = function(event)
  local neutral_force = game.forces['neutral']
  local default_surface = game.surfaces['nauvis']
  for _, player in pairs(game.players) do
    if math.random(100) > 10 then
      if player.character and player.afk_time <= MAX_AFK_TIME and not player.cheat_mode and player.surface == default_surface then
        local position = default_surface.find_non_colliding_position('small-electric-pole', player.position, 6, 3)
        if position then
          default_surface.create_entity{name = 'fish', position = position, force = neutral_force}
        end
      end
    end
  end
end

mod.events = {
  on_player_mined_entity = check_fish,
  on_robot_mined_entity = check_fish
}

return mod
