return {
  enemies = {
    {
      name = 'biter',
      sizes = {'small', 'medium', 'big', 'behemoth'}
    },
    {
      name = 'spitter',
      sizes = {'small', 'medium', 'big', 'behemoth'}
    },
    {
      name = 'biter-spawner'
    },
    {
      name = 'spitter-spawner'
    },
    {
      name = 'worm-turret',
      sizes = {'small', 'medium', 'big'}
    }
  },

  fish_tick = 60 * 60 * 5
}
