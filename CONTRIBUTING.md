# Contributing

## About translation

Please note the mod is in the process of being internationalized.

## Coding conventions

We optimize for readability:

* We indent using tabs, however spaces are also acceptable
* Read [a note about Git commit messages](https://tbaggery.com/2008/04/19/a-note-about-git-commit-messages.html)
* Each branch is divided into versions of the game Factorio x.x (e.g., 0.17)

## Prerequisites

We recommend several tools build the mod, including:

* [Git](https://git-scm.com) — distributed version control system
* [jq](https://stedolan.github.io/jq/) — command-line JSON processor

## Extensions

If you are using [Visual Studio code](https://code.visualstudio.com), we recommend:

* [Lua](https://marketplace.visualstudio.com/items?itemName=keyring.Lua)
* [vscode-lua](https://marketplace.visualstudio.com/items?itemName=trixnz.vscode-lua)
* [Factorio Lua API autocomplete](https://marketplace.visualstudio.com/items?itemName=svizzini.factorio-lua-api-autocomplete)
* [TODO Highlight](https://marketplace.visualstudio.com/items?itemName=wayou.vscode-todo-highlight)
* [markdownlint](https://marketplace.visualstudio.com/items?itemName=DavidAnson.vscode-markdownlint)
* [Guides](https://marketplace.visualstudio.com/items?itemName=spywhere.guides)

### For others IDE:

* For Vim, [Syntastic](https://github.com/vim-syntastic/syntastic) contains [luacheck checker](https://github.com/vim-syntastic/syntastic/wiki/Lua%3A---luacheck);
* For Sublime Text 3 there is [SublimeLinter-luacheck](https://packagecontrol.io/packages/SublimeLinter-luacheck) which requires [SublimeLinter](https://sublimelinter.readthedocs.io/en/latest/);
* For Atom there is [linter-luacheck](https://atom.io/packages/linter-luacheck) which requires [AtomLinter](https://github.com/steelbrain/linter);
* For Emacs, [Flycheck](http://www.flycheck.org/en/latest/) contains [luacheck checker](http://www.flycheck.org/en/latest/languages.html#lua);
* For Brackets, there is [linter.luacheck](https://github.com/Malcolm3141/brackets-luacheck) extension;
